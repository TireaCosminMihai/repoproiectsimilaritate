#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <algorithm>
using namespace std;

bool isPrefix(std::string_view prefix, std::string_view word)
{
	return prefix == word.substr(0, prefix.size());
}

bool isSufix(std::string const& word, std::string const& sufix)
{
	if (word.size() >= sufix.size())
	{
		return (0 == word.compare(word.size() - sufix.size(), sufix.size(), sufix));
	}
	else
	{
		return false;
	}
}

string verificarecuvant(string el)
{
	vector<string> prefixe = { "aero","agro","anti","auto","audio","bio","bi","contra","dez","echi","ex","extra","filo","foto",
		"hidro","hipo","intre","in","mega","micro","mini","mono","multi","ne","non","pan","para","pico","poli","pre","semi",
		"sub","supra","super","tehno","ultra" };
	vector<string> sufixe = { "ar","arie","ie","it","in","ind ","se","re","an","ca","ator","eala","ic" };
	vector<string> prepozitii = { "care","pentru","a","intre","in","de","la","langa","fara","prin","cu","prea","are","si","sa" };
	vector <string> punctuatie = { "...",".",",","'","!","?","(",")","@",":",";" };
	transform(el.begin(), el.end(), el.begin(), ::tolower);
	int ok = 1;
	for (string s : prepozitii)
		if (el == s && el.size() == s.size())
			ok = 0;

	int nrprefix = 0;
	for (string s : prefixe)
	{
		if (isPrefix(s, el) && nrprefix < 1)
		{
			el.erase(0, s.size());
			nrprefix++;
		}
	}
	int nrsufix = 0;
	for (string s : sufixe)
	{
		if (isSufix(el, s) && nrsufix < 1)
		{
			el.erase(el.size() - s.size(), el.size());
			nrsufix++;
		}
	}

	for (string s : punctuatie)
	{
		if (el.substr(0, 1) == s)
			el.erase(0, 1);

		if (isSufix(el, s))
			el.erase(el.size() - s.size(), el.size());

		if (el == s && el.size() == s.size())
			ok = 0;
	}

	if (ok == 1)
		return el;
	if (ok == 0)
		return "nu";
}

void creareavectoruluireuniune(vector<string> stringD1, vector<string> stringD2, vector<string>& stringD3)
{
	for (string s : stringD1)
		stringD3.push_back(s);
	for (string s : stringD2)
		stringD3.push_back(s);

}
void citire(string input_file_name, vector<string>& stringD, int& nrcuvinte)
{
	string el;
	nrcuvinte = 0;
	ifstream file_in(input_file_name);
	if (file_in.is_open())
	{
		while (file_in)
		{
			file_in >> el;
			if (el != "," && el != "." && el != "?" && el != "!" && el != ":" && el != ";" && el != "(" && el != ")")
			{
				nrcuvinte++;
			}
			if (verificarecuvant(el) != "nu")
				stringD.push_back(verificarecuvant(el));
		}
		stringD.pop_back();
		nrcuvinte--;
		file_in.close();
	}
	else
	{
		cout << "Fisierul nu poate fi deschis!" << endl;
	}
}

void automatul(vector<string> D3, vector<string> D1, vector<string> D2, int& d1minusd2, int& d2minusd1, int& d1comund2)
{
	for (string cuvantd3 : D3)
	{
		int gasit = 0;
		for (string cuvantd1 : D1)
		{
			int ok = 0;
			if (cuvantd3 == cuvantd1 || cuvantd1 == cuvantd3)
			{
				gasit = 1;
				for (string cuvantd2 : D2)
				{
					if (cuvantd3 == cuvantd2 || cuvantd2 == cuvantd3)
					{
						ok = 1;
						break;
					}
				}
				if (ok == 0)
				{
					d1minusd2++;
					break;
				}
			}
		}
		if (gasit == 0)
			d2minusd1++;
	}

	for (string cuvantd1 : D1)
		for (string cuvantd2 : D2)
		{
			if (cuvantd1 == cuvantd2 || cuvantd2 == cuvantd1)
			{
				d1comund2++;
				break;
			}
		}

}
void afisaretextdupaprelucrare(vector<string> stringD1, vector<string> stringD2, vector<string> stringD3)
{
	int optiune = 1;
	while (optiune != 0)
	{
		cout << endl;
		cout << "0.Pentru iesire din meniul de afisare stringuri\n";
		cout << "1.Afisare primul text dupa prelucrare\n";
		cout << "2.Afisare al doilea text dupa prelucrare\n";
		cout << "3.Afisare primul si al doilea text dupa prelucrare\n";
		cout << "Optiunea este: ";
		cin >> optiune;

		if (optiune == 1)
		{
			for (string s : stringD1)
			{
				cout << s << " ";
			}
			cout << endl;
		}
		else
			if (optiune == 2)
			{
				for (string s : stringD2)
				{
					cout << s << " ";
				}
				cout << endl;
			}
			else
				if (optiune == 3)
				{
					for (string s : stringD1)
					{
						cout << s << " ";
					}
					cout << endl;

					for (string s : stringD2)
					{
						cout << s << " ";
					}

					cout << endl;
				}
	}
}
void listaoptiuni()
{
	cout << endl;
	cout << "0.Terminare program\n";
	cout << "1.Afisare vectori\n";
	cout << "2.Afisarea numarului de cuvinte din textul1 inainte de prelucrare\n";
	cout << "3.Afisarea numarului de cuvinte din textul2 inainte de prelucrare prelucrare\n";
	cout << "4.Afisarea numarului de cuvinte din textul1 dupa de prelucrare\n";
	cout << "5.Afisarea numarului de cuvinte din textul2 dupa de prelucrare prelucrare\n";
	cout << "6.Afisarea numarul de cuvinte care se afla excluiv in textul 1\n";
	cout << "7.Afisarea numarul de cuvinte care se afla excluiv in textul 2\n";
	cout << "8.Afisarea numarul de cuvinte comune din textul1 si textul 2\n";
	cout << "9.Afisarea procentuala asupra similaritatii celor 2 texte!\n";
}
void comanda(vector<string>& stringD1, vector<string>& stringD2, vector<string>& stringD3, int& nrcuvinteD1, int& nrcuvinteD2, int d1minusd2, int d2minusd1, int d1comund2)
{
	int optiune;
	listaoptiuni();
	cin >> optiune;
	while (optiune != 0)
	{
		switch (optiune)
		{
		case 1:
			afisaretextdupaprelucrare(stringD1, stringD2, stringD3);
			listaoptiuni();
			cin >> optiune;
			break;
		case 2:
			cout << "Documentul 1 :" << nrcuvinteD1;
			listaoptiuni();
			cin >> optiune;
			break;
		case 3:

			cout << "Documentul 2 :" << nrcuvinteD2;
			listaoptiuni();
			cin >> optiune;
			break;
		case 4:
			cout << stringD1.size();
			listaoptiuni();
			cin >> optiune;
			break;
		case 5:
			cout << stringD2.size();
			listaoptiuni();
			cin >> optiune;
			break;
		case 6:
			cout << d1minusd2;
			listaoptiuni();

			cin >> optiune;
			break;
		case 7:
			cout << d2minusd1;
			listaoptiuni();
			cin >> optiune;
			break;
		case 8:
			cout << d1comund2;
			listaoptiuni();
			cin >> optiune;
			break;
		}
		if (optiune == 9)
		{
			int valoared1minusd2 = (int)((double)d1minusd2 / stringD3.size() * 100);
			int valoared2minusd1 = (int)((double)d2minusd1 / stringD3.size() * 100);
			int valoared1comund2 = (int)((double)d1comund2 / stringD3.size() * 100);
			if (valoared1comund2 > 20)
			{
				cout << "Documentele sunt similare in procent de " << valoared1comund2 << "%!";
			}
			else
				if (valoared2minusd1 <= 20 && valoared1minusd2 <= 20)
				{
					cout << "Documentele sunt similare in procent de " << valoared1comund2 << "%!";
				}
				else
				{
					cout << "Documentele sunt diferite avand un procent acceptabil de similaritate de " << valoared1comund2 << "%!";
				}
			optiune = 0;
		}
	}
}

int main()
{
	vector<string> stringD1, stringD2;
	int nrcuvinteD1, nrcuvinteD2;
	citire("Text1.txt", stringD1, nrcuvinteD1);
	citire("Text2.txt", stringD2, nrcuvinteD2);

	vector <string> stringD3;
	creareavectoruluireuniune(stringD1, stringD2, stringD3);

	int d1minusd2 = 0, d2minusd1 = 0, d1comund2 = 0;
	automatul(stringD3, stringD1, stringD2, d1minusd2, d2minusd1, d1comund2);
	cout << "Program similaritate !";
	comanda(stringD1, stringD2, stringD3, nrcuvinteD1, nrcuvinteD2, d1minusd2, d2minusd1, d1comund2);
	return 0;
}